﻿// 5_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <Windows.h>
#define SIZE_LEN 256

void getWords(char *pArr, char **ppArr)
{
    int nActualPointer = 0;
    printf("Enter the string\n");
    int nCurrentPosition = 0;
    do
    {
        *(pArr + nCurrentPosition) = getchar();

        if (*(pArr + nCurrentPosition) == ' ')
        {
            *(ppArr + nActualPointer) = (pArr + nCurrentPosition + 1);//+1 - заглушка на пробел
            nActualPointer++;
        }
    } while (*(pArr + nCurrentPosition++) != '\n');
}

void printWord(char* ch)
{
    int nCurrensPosition = 0;
    while ((*(ch + nCurrensPosition) != ' ') && (*(ch + nCurrensPosition) != '\n'))
        printf("%c", *(ch + nCurrensPosition++));
}

int _tmain(int argc, _TCHAR* argv[])
{
    char charArr[SIZE_LEN] = "";
    char *pArr[SIZE_LEN] = { charArr, 0 };
    getWords(charArr, (pArr + 1));//да, хочу передать именно второй элемент массива, так как изначально ясно, что начало массива и первый указатель будут начинаться с одной позиции
    int countWords = 0;
    
    while (*(pArr + countWords++));//узнаем количество слов в строке
    
    countWords--;//так как в результате прохождения цикла мы получаем позицию конца строки в качестве последнего слова, то надо ставить эту заглушку для настоящего количества слов

    for (int n = 0; n < countWords; n++)
    {
        int nRandom = rand() % countWords;
        do 
        {
            nRandom = rand() % countWords;
        } while (pArr[nRandom] == NULL);

        printWord(pArr[nRandom]);
        pArr[nRandom] = NULL;

    }

    return 0;
}