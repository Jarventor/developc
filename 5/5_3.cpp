﻿// 5_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <Windows.h>
#define SIZE_LEN 256
#define PATH "G:/Labs/all_solutions/Debug/input.txt"

void mixString(char *chIn)
{
    char *beginWord = chIn + 1;//+1 - заглушка, что б первая буква оставалась на своем месте
    char *endWord = beginWord;
    char chBuf = 0;

    while ((*(endWord) != ' ') && (*(endWord++) != '\n'));

    //-- - заглушка, что б последняя буква оставалась на своем месте
    endWord--;

    while ((--endWord) > (beginWord))
    {
        int random = rand() % 2;
        if (random == 1)
        {
            chBuf = *endWord;
            *endWord = *beginWord;
            *beginWord = chBuf;
        }
    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    char charArr[SIZE_LEN] = { 0 };

    unsigned short int nString = 0;

    char* read = NULL;
    FILE* in;
    fopen_s(&in, PATH, "r+");

    while (fgets(charArr, sizeof(charArr), in))
    {
        mixString(charArr);

        for (int n = 0; n < strlen(charArr); n++)
        {
            if (charArr[n] == ' ')
                mixString(&charArr[n + 1]);
        }
        printf("%s\n", charArr);

        for (int n = 0; n < SIZE_LEN; n++)
            charArr[n] = 0;
    }
    fclose(in);

    return 0;
}


