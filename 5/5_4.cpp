﻿// 5_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <Windows.h>
#define SIZE_LEN 256
#define PATH "G:/Labs/all_solutions/Debug/input.txt"

void getWords(char *pArr, char **ppArr)
{
    int nActualPointer = 0;
    for (int nCurrentPosition = 0;( (*(pArr + nCurrentPosition) != '\n') && (*(pArr + nCurrentPosition) != '\0')); nCurrentPosition++)
    {
        if (*(pArr + nCurrentPosition) == ' ')
        {
            *(ppArr + nActualPointer) = (pArr + nCurrentPosition + 1);//+1 - заглушка на пробел
            nActualPointer++;
        }
    }
}

void printWord(char* ch)
{
    int nCurrensPosition = 0;
    while ((*(ch + nCurrensPosition) != ' ') && (*(ch + nCurrensPosition) != '\n') && (*(ch + nCurrensPosition) != '\0'))
        printf("%c", *(ch + nCurrensPosition++));
    printf(" ");
}

int _tmain(int argc, _TCHAR* argv[])
{
    char charArr[SIZE_LEN] = { 0 };

    unsigned short int nString = 0;

    char* read = NULL;
    FILE* in;
    fopen_s(&in, PATH, "r+");

    while (fgets(charArr, sizeof(charArr), in))
    {
        char *pArr[SIZE_LEN] = { charArr, 0 };
        getWords(charArr, (pArr + 1));
        int countWords = 0;

        while (*(pArr + countWords++));

        countWords--;

        for (int n = 0; n < countWords; n++)
        {
            int nRandom = rand() % countWords;
            do
            {
                nRandom = rand() % countWords;
            } while (pArr[nRandom] == NULL);

            printWord(pArr[nRandom]);
            pArr[nRandom] = NULL;

        }
        printf("\n");
    }
    return 0;
}