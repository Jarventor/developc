// 5_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <Windows.h>
#define SIZE 64

void clean(char *pArr)
{
    for (int i = 0; i < SIZE; i++)
        for (int j = 0; j < SIZE; j++)
            *(pArr + i * SIZE + j) == 0;
}

void print(char *pArr)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
            printf("%c", *(pArr + i * SIZE + j));

        printf("\n");
    }
}

void fill(char *pArr)
{
    int nBuf = SIZE / 2;
    int selection = 0;
    char chArr[SIZE][SIZE];

    for (int n = 0; n < nBuf; n++)
        for (int j = 0; j < nBuf; j++)
        {
            if ((rand() % 2) == 1)
                chArr[n][j] = '*';
            else
                chArr[n][j] = ' ';
        }

    for (int n = 0; n < nBuf; n++)
        for (int j = 0; j < nBuf; j++)
        {
        chArr[n][SIZE - j - 1] = chArr[n][j];
        chArr[SIZE - n - 1][j] = chArr[n][j];
        chArr[SIZE - n - 1][SIZE - j - 1] = chArr[n][j];
        }

    for (int n = 0; n < SIZE; n++)
        for (int j = 0; j < SIZE; j++)
        {
        *(pArr + n * SIZE + j) = chArr[n][j];
        }

}
int _tmain(int argc, _TCHAR* argv[])
{
    while (true)
    {
        char charArr[SIZE][SIZE] = { 0 };
        fill(charArr[0]);
        print(charArr[0]);
        Sleep(3000);
        system("cls");
    }        
	return 0;
}

