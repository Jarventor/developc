#include "stdafx.h"
#include <cstdlib>
#include "Windows.h"
#include <stdio.h>
#include "string.h"

#define DEFAULT_NUM_INTERVAL 48
#define ARR_SIZE 45
#define INTERVAL_SIM 80
#define UPPEND_BOUND_OF_NUMBERS 10

void arrRandom(char* char_arr, int sizeArr)
{
    for (int n = 0; n < sizeArr; n++)
        *(char_arr++) = char(rand()%(INTERVAL_SIM)+DEFAULT_NUM_INTERVAL);
}

bool compareChecking (int number, int down_line = 0, int up_line = UPPEND_BOUND_OF_NUMBERS)
{
    if ((number < down_line + DEFAULT_NUM_INTERVAL)||(number >= up_line + DEFAULT_NUM_INTERVAL))
        return false;
    else 
        return true;
}
int main_2_4()
{
    char arr [ARR_SIZE] = "";
    const int sizeArr = sizeof(arr)-1;
    arrRandom(arr, sizeArr);
    printf("%s\n", arr);
    char chTemp;

    for(int nNumber = strlen(arr)-1, nSymbol = 0; nSymbol < nNumber;)
    {
        while(!compareChecking(arr[nSymbol]))
            nSymbol++;

        while(compareChecking(arr[nNumber]))
            nNumber--;

        if(nSymbol < nNumber)
        {
            chTemp = arr[nSymbol];
            arr[nSymbol] = arr[nNumber];
            arr[nNumber] = chTemp;
        }
    }
    printf("%s\n", arr);
    return 0;
}