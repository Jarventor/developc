﻿#include "stdafx.h"
#define MAX 256

int _tmain(int argc, _TCHAR* argv[])
{
    char buf[MAX];
    short int counts[MAX] = { 0 };

    printf("Insert the line\n");
    scanf_s("%s", buf, MAX);

    for (int i = 0; buf[i]; i++)
        counts[buf[i]]++;

    for (int i = 0; i < MAX; i++)
        if (counts[i])
            printf("%c - %d\n", i, counts[i]);
    return 0;
}
