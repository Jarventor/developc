#include "stdafx.h"
#include <cstdlib>

#define SUM_OF_NUMBERS 10
#define LOWER_BOUNDS_OF_NUMBERS 48
#define SUM_OF_LETTER_GROW 26
#define LOWER_BOUND_OF_LETTER_GROW 65
#define SUM_OF_LETTER_LITTLE 26
#define LOWER_BOUND_OF_LETTER_LITTLE 97
#define ARR_SIZE 10

char chRandom(char* char_arr, int index = 0)
{
    int size = 0;
    char *t = char_arr;
    
    while (*t)
    {
        *t++;
        size++;
    }
    int x, y;
    
    switch (index)
    {
    case 0:
        x = LOWER_BOUND_OF_LETTER_LITTLE;
        y = SUM_OF_LETTER_LITTLE;
        break;
    case 1:
        x = LOWER_BOUND_OF_LETTER_GROW;
        y = SUM_OF_LETTER_GROW;
        break;
    case 2:
        x = LOWER_BOUNDS_OF_NUMBERS;
        y = SUM_OF_NUMBERS;
    }    
    return (rand()%(y) + x);
}

int main_2_5()
{
    char arr[ARR_SIZE] = "";
    int sizeArr = sizeof(arr)-1;
    
    for (int k = 0; k < ARR_SIZE; k++)
    {
        for (int n = 0; n < sizeArr; n++)
            arr[n] = chRandom(&arr[n], rand()%(3));
        printf("%s\n", arr); 
    }
return 0;
}
