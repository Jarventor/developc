#include "stdafx.h"
#include <cstdlib>
#include "Windows.h"
#include <time.h>
#include <stdio.h>

#define SUM_OF_NUMBERS 10
#define LOWER_BOUNDS_OF_NUMBERS 48
#define SUM_OF_LETTER_GROW 26
#define LOWER_BOUND_OF_LETTER_GROW 65
#define SUM_OF_LETTER_LITTLE 26
#define LOWER_BOUND_OF_LETTER_LITTLE 97
#define ARR_SIZE 45

void delSym(char* arr)
{
    while(*(arr++))
        *(arr - 1) = *arr;
}

char* findSpacePosition(char* ch)
{
    while(*(ch + 1))
    {
        if(*ch == ' ') 
        {
            if(*(ch + 1) == ' ')
                return (ch + 1);
        }

        ch++;
    }
    return NULL;
}

char charRandom(char* char_arr, int index = 0)
{
    int size = 0;
    int x, y;
    
    switch (index)
    {
    case 0:
        x = LOWER_BOUND_OF_LETTER_LITTLE;
        y = SUM_OF_LETTER_LITTLE;
        break;
    case 1:
        x = LOWER_BOUND_OF_LETTER_GROW;
        y = SUM_OF_LETTER_GROW;
        break;
    case 2:
        x = LOWER_BOUNDS_OF_NUMBERS;
        y = SUM_OF_NUMBERS;
    case 3:
        return 32;
    }
    return (rand()%(y) + x);
}

int main_2_6()
{
    char arr[ARR_SIZE] = "";
    
    for (int n = 0; n + 1 < ARR_SIZE; n++)
        arr[n] = charRandom(&arr[n], rand()%(4));

    printf("stroka = '%s'\n", arr);
    char* string = arr;

    while(findSpacePosition(arr) != NULL)
    {
        string = findSpacePosition(arr);
        delSym(string);
    }

    if(arr[0] == ' ')
        delSym(arr);

    if(arr[strlen(arr) - 1] == ' ')
        delSym(&arr[strlen(arr) - 1]);

    printf("stroka = '%s'\n", arr);
    return 0;
}
