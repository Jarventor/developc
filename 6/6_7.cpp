﻿// 6_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

#define HEIGHT 9
#define WIDTH  27
#define SPACER '#'

char labyrinth[HEIGHT][WIDTH] = {   "##########################", \
                                    "#          #  #          #", \
                                    "########## #  #          #", \
                                    "#          #  #######  ###", \
                                    "# #######  #             #", \
                                    "#      ##  #  ######   ###", \
                                    "#####  ##  #  #        ###", \
                                    "       ##     #     ######", \
                                    "##########################" };

int labyrinthPrint(int x, int y)
{
    if ((x == 0) || (x >= HEIGHT) || (y == 0) || (y >= WIDTH))
        return -1;
    for (int j = 0; j < HEIGHT; j++)
    {
        for (int i = 0; i < WIDTH; i++)
        {
            if ((x != j) || (y != i))
                printf("%c", labyrinth[j][i]);
            else
                printf("%c", 'X');
        }
        printf("\n");
    }
    printf("\n%i,%i\n", x, y);
    return 0;
}

bool drive(int* x, int* y, int trend)
{
    int mask1 = 0, mask2 = 0;
    switch (trend)
    {
    case 0:
        mask2--;
        break;
    case 1:
        mask1--;
        break;
    case 2:
        mask2++;
        break;
    case 3:
        mask1++;
        break;
    }
    
    if (labyrinth[*x + mask1][*y + mask2] != SPACER)
    {
        *x = *x + mask1;
        *y = *y + mask2;
        return false;
    }
    return true;

}

int _tmain(int argc, _TCHAR* argv[])
{
    int x = HEIGHT / 2;
    int y = WIDTH / 2;
    char* marker = 0;
    bool findWall = true;
    int nBuf = 0;
    int trend = 0;                                 //1-left, 2-up, 3-rigth, 4-down
    int u = 0, l = 0, d = 0, r = 0;
    while (!labyrinthPrint(x, y))
    {                  
        if (findWall)
        {
            if (drive(&x, &y, 0))
                findWall = false;
        }
        else
        {
            nBuf = trend;
            switch (trend)
            {
            case 0:
                if (labyrinth[x - 1][y - 1] != SPACER)
                    trend = 1;
             
                if (drive(&x, &y, nBuf))
                    trend = 3;

                break;

            case 1:
                if (labyrinth[x - 1][y + 1] != SPACER)
                    trend = 2;

                if (drive(&x, &y, nBuf))
                    trend = 0;

                break;
            case 2:
                if (labyrinth[x + 1][y + 1] != SPACER)
                    trend = 3;

                if (drive(&x, &y, nBuf))
                    trend = 1;

                break;

            case 3:
                if (labyrinth[x + 1][y - 1] != SPACER)
                    trend = 0;

                if (drive(&x, &y, nBuf))
                    trend = 2;

                break;
            }
        } 
        Sleep(100);
        system("cls");
    }
    printf("the end");
	return 0;
}