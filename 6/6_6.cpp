// 6_6.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdlib.h>

int fib2(int a, int b, int num)
{
    if (num == 1)
        return b;
    else
        return fib2(b, a + b, num - 1);
}

int fib(int num)
{
    return fib2(0, 1, num);
}

int _tmain(int argc, _TCHAR* argv[])
{
    int itemNumber = 0;
    puts("press num \n");
    scanf_s("%i", &itemNumber);
    int rez = fib(itemNumber);
    printf("%i - %i\n", itemNumber, rez);
    return 0;
}
