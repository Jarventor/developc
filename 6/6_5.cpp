﻿// 6_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 100
#define TIMECONST (CLOCKS_PER_SEC / 1000)


int fib(int N)
{
    if (N == 1 || N == 2)
        return 1;
    else return fib(N - 2) + fib(N - 1);
}

int _tmain(int argc, _TCHAR* argv[])
{
    int nStart = 0;
    int nEnd = 0;
    char outputFormat = ' ';
    puts("press num start, num end, output format(P or M)(example: 1,40,M)\n");
    scanf_s("%i,%i,%C", &nStart, &nEnd, &outputFormat, 1);
    bool outFormat = false;

    if (outputFormat == 'M')
        outFormat = true;
    else if (outFormat == 'P')
        outFormat = false;
    else
        return 0;

    FILE* out;
    fopen_s(&out, "output.txt", "a+");

    for (int nBuf = nStart; nBuf < nEnd; nBuf++)
    {
        printf("num %i is ", nBuf);
        clock_t start = clock();
        int rez = fib(nBuf);
        clock_t end = clock();
        int currentTime = (end - start) / TIMECONST;
        
        if (outFormat)
            printf("%i milliseconds\n", currentTime);
        else
            fprintf(out, "%i - %i\n", nBuf, currentTime);
    }
	return 0;
}

