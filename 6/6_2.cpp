// 6_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define BEGIN 2
#define END 1000000

void sirakuza(unsigned long int *num)
{
    if (*num % 2 == 0)
        *num = *num / 2;
    else
        *num = *num * 3 + 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
    int numMax = 0, depthMax = 0;

    for (unsigned long int counter = BEGIN; counter < END; counter++)
    {
        unsigned long int num = counter;
        int depth = 0;
        
        while (num != 1)
        {
            sirakuza(&num);
            depth++;
        }

        if (depth > depthMax)
        {
            depthMax = depth;
            numMax = counter;
        }
    }
    printf("num is %i, depth is %i", numMax, depthMax);
	return 0;
}

