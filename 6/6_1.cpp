﻿// 6_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <malloc.h>

#define ARRSIZE 243

void reWrite(char *arr, int sizeRoot)
{
    char *it = (char *)calloc(sizeRoot * sizeRoot, sizeof(char));
    
    for (int n = 0; n < sizeRoot * sizeRoot; n++)
        *(it + n) = *(arr + n);

    for (int n = 0; n < ARRSIZE * ARRSIZE; n++)
        *(arr + n) = '\0';
    
    for (int j = 0; j < sizeRoot; j++)
    {
        for (int i = 0; i < sizeRoot; i++)
        {
            *(arr + (j)* 3 * sizeRoot + sizeRoot + i) = *(it + j * sizeRoot + i);
            *(arr + (j + sizeRoot) * 3 * sizeRoot + i) = *(it + j * sizeRoot + i);
            *(arr + (j + sizeRoot) * 3 * sizeRoot + sizeRoot + i) = *(it + j * sizeRoot + i);
            *(arr + (j + sizeRoot) * 3 * sizeRoot + sizeRoot + sizeRoot + i) = *(it + j * sizeRoot + i);
            *(arr + (j + sizeRoot + sizeRoot) * 3 * sizeRoot + sizeRoot + i) = *(it + j * sizeRoot + i);//дальше не стал сокращать, что б код выглядел понятным
        }
    }
}

void printf(char *arr)
{
    for (int n = 0; n < ARRSIZE * ARRSIZE; n++)
    {
        if (n % ARRSIZE == 0)
            printf("\n");

        if (arr[n] == '*')
            printf("*");
        else
            printf(" ");


    }
}

int _tmain(int argc, _TCHAR* argv[])
{
    char ar[ARRSIZE * ARRSIZE] = "*";
    
    for(int n = 1; n < ARRSIZE; n = 3 * n)
        reWrite(ar, n);

    printf(ar);
	return 0;
}

