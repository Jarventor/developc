// 6_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define DIGITALCONST 48

void getString(int number, char *it)
{
    *it-- = char(number % 10 + DIGITALCONST);
    number = number / 10;
    if (number >= 1)
        getString(number, it);
}

int razryad(int chislo)
{
    int razrad = 0;
    
    while (chislo > 9)
    {
        chislo /= 10;
        razrad++;
    }
    return razrad;
}

int _tmain(int argc, _TCHAR* argv[])
{
    int number = 0;
    scanf_s("%i", &number);
    char arr[DIGITALCONST] = "";
    getString(number, &arr[razryad(number)]);
    printf("this num is %s", arr);
	return 0;
}

