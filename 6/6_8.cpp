﻿// 6_8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <string.h>
#define MAX 256
#define DIGITALCONST 48

void delChar(char* ch)
{
    do
    {
        *(ch) = *(ch + 1);
        ch++;
    } while (*ch);
}

bool isDigital(char ch)
{
    if (ch > DIGITALCONST)
        if (ch < DIGITALCONST + 10)
            return true;

    return false;
}

char partition(char* buf, char* expr1, char* expr2)
{
    int flag = 0;
    delChar(buf);
    int count = (int)strlen(buf);
    delChar((buf + count - 1));
    char operation = 0;
    int currentPos = 0;
    char* p = 0;

    for (int n = 0; *(buf + n); n++)
    {
        if (*(buf + n) == '(')
            flag++;

        if (flag == 0)
        {
            if (!isDigital(*(buf + n)))
            {
                operation = *(buf + n);
                p = (buf + n);
                break;
            }
        }

        if (*(buf + n) == ')')
            flag--;
    }
    for (int n = 0; n < count; n++)
    {
        if ((buf + n) < p)
        {
            *(expr1 + currentPos) = *(buf + n);
            currentPos++;
        }
        else  if ((buf + n) == p)
        {
            currentPos = 0;
        }
        else
        {
            *(expr2 + currentPos) = *(buf + n);
            currentPos++;
        }
    }
    return operation;
}

int eval(char* buf)
{
    if (*buf != '(')
        return atoi(buf);

    char expr1[MAX] = "", expr2[MAX] = "";

    switch (partition(buf, expr1, expr2))
    {
    case '+':
        return eval(expr1) + eval(expr2);
    case '-':
        return eval(expr1) - eval(expr2);
    case '*':
        return eval(expr1) * eval(expr2);
    case '/':
        return eval(expr1) / eval(expr2);
    }
}


int main(int argc, char* argv[])
{
    int res = eval(argv[1]);
    printf("%d\n", res);
    return 0;
}

