// 6_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <time.h>

#define DIGITALCONST 48
#define TIMECONST (CLOCKS_PER_SEC / 1000)

int standartWay(long int* ptr, unsigned int size)
{
    int rezult = 0;
    
    for (unsigned long long int nBuf = 0; nBuf < size; nBuf++)
        rezult += ptr[nBuf];

    return rezult;
}

int recursion(long int *buf, long int size)
{
    if (size == 1)
        return buf[0];
    else
        return recursion(buf, size / 2) + recursion(buf + size / 2, size - size / 2);
}

int _tmain(int argc, char** argv)
{
    int exp = atoi(argv[1]);
    long int degree = pow(2.0, exp);
    degree = degree * degree;

    long int *ptr = (long int *)calloc(degree, sizeof(long int));
    
    for (unsigned long int nBuf = 0; nBuf < degree; nBuf++)
    {
        ptr[nBuf] = rand() % (DIGITALCONST);
    }
    
    int standartRez = 0;
    int recursiveRez = 0;

    clock_t start = clock();
    standartRez = standartWay(ptr, degree);
    clock_t end = clock();
    int standartNum = (end - start)  / TIMECONST;

    start = clock();
    recursiveRez = recursion(ptr, degree);
    end = clock();
    int recursiveNum = (end - start) / TIMECONST;


    printf(" standart rezult = %i, recursiverezult = %i", standartNum, recursiveNum);
	return 0;
}

