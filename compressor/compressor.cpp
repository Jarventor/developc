﻿// compressor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string.h>
#include <malloc.h>
#include <sys/stat.h>

#define PATH "D:/6_8.cpp"
#define PATH2 "D:/input.txt2"
#define PATHKEY "D:/input.tkey"


#define COUNTLET 256
#define DIGITALCONST 48
#define SYS 32

void intToBin(int num, unsigned char *ch)
{
    *ch = (num % 2) + DIGITALCONST;

    if (num / 2 >= 1)
    {
        intToBin(num / 2, (ch + 1));
    }
}

void reading(unsigned char* buf, unsigned long long int* count, char *mode = "r")
{
    printf("reading %s ", PATH);

    FILE* in;
    *count = 0;

    if ((fopen_s(&in, PATH, mode)) != NULL)
    {
        printf("error 1\n");
    }
    else
    {
        while (fread((buf + (*count)), sizeof(unsigned char), 1, in))
            (*count)++;
    }

    fclose(in);
    printf("successful\n");
}

void writing(unsigned char* buf, char *path = PATH2, char *mode = "a", int size = 0)
{
    FILE *out;
    int count = 0;

    if ((fopen_s(&out, path, mode)) != NULL)
    {
        printf("error 3\n");
    }
    else
    {
        for (unsigned long long int n = 0; n < size; n++)
        {
            fwrite(&buf[count], sizeof(unsigned char), 1, out);
            count++;
        }

        if (size = 0)
            printf(" the file become NULL ");
    }

    fclose(out);
    printf("successful\n");

}

unsigned char* sortingByFrequency(unsigned char* buf, unsigned long long int* count, int* countChars)
{
    int maxValues = 0;
    unsigned char *p = (unsigned char *)calloc(*count, sizeof(unsigned char));
    int *pBuf = (int *)calloc(COUNTLET, sizeof(int));
    *countChars = 0;

    for (int i = 0; buf[i]; i++)
        pBuf[buf[i]]++;

    do
    {
        maxValues = 0;
        for (int i = 0; i < COUNTLET; i++)
            if (pBuf[i] > maxValues)
                maxValues = pBuf[i];

        for (int i = 0; i < COUNTLET; i++)
            if ((pBuf[i] == maxValues) && (pBuf[i] != 0))
            {
            p[*countChars] = i;
            printf("%c - %d\n", unsigned char(i), maxValues);
            (*countChars)++;
            pBuf[i] = 0;
            }
    } while (maxValues > 0);

    return p;
}

int _tmain(int argc, _TCHAR* argv[])
{
    printf("program started\n");

    struct stat st;
    stat(PATH, &st);
    int size = (int)st.st_size;

    unsigned char *chArr = (unsigned char *)calloc(size, sizeof(unsigned char));
    unsigned long long int count = 0;
    reading(chArr, &count);
    int countChars = 0;
    unsigned char* pSortBFrec = sortingByFrequency(chArr, &count, &countChars);
    int depth = 0;

    unsigned char *chOut = (unsigned char *)calloc((size * size), sizeof(unsigned char));
    unsigned char *p = chOut;
    unsigned long long int nBuf = 0;
    int n = 0;

    printf("writing 1/4(%s)...", PATHKEY);
    writing(NULL, PATHKEY, "w", 0);

      printf("writing 2/4(%s)...", PATHKEY);
    writing(pSortBFrec, PATHKEY, "w", countChars);

    int nTemp = 0;
    int j = 0;
    int i = 0;
    unsigned char byte_mask[8] = { 128, 64, 32, 16, 8, 4, 2, 1 }; //набор битовых масок

    for (i = 0; i < size; i++)
    {
        for (n = 0; n < countChars; n++)
        {
            nTemp = nBuf % 8;
            p[nBuf / 8] |= byte_mask[nTemp];
            nBuf++;

            if (pSortBFrec[n] == chArr[i])
                break;

        }
        nBuf++;
        //printf("%c", chArr[i]);
    }

    printf("\nwriting 3/4(%s)...", PATH2);
    writing(NULL, PATH2, "w");

    int chOutSize = 0;

    for (; chOut[chOutSize]; chOutSize++);

    printf("\nwriting 4/4(%s)...", PATH2);
    writing(chOut, PATH2, "ab", chOutSize);

    printf("the program has worked successfully\n");
    return 0;
}

