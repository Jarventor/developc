﻿// decompressor.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <string.h>
#include <malloc.h>
#include <sys/stat.h>

#define PATH3 "D:/input3.txt"
#define PATH2 "D:/input.txt2"
#define PATHKEY "D:/input.tkey"

#define COUNTLET 256
#define DIGITALCONST 48
#define SYS 32

unsigned char byte_mask[8] = { 128, 64, 32, 16, 8, 4, 2, 1 }; //набор битовых масок

void intToBin(int num, char *ch)
{
    *ch = (num % 2) + DIGITALCONST;

    if (num / 2 >= 1)
    {
        intToBin(num / 2, (ch + 1));
    }
}

void delChar(char* ch)
{
    do
    {
        *(ch) = *(ch + 1);
        ch++;
    } while (*ch);
}

void reading(char* buf, unsigned long long int* count, const char *path = PATH2, char *mode = "rb")
{
    printf("reading %s... ", path);

    FILE* in;
    *count = 0;

    if ((fopen_s(&in, path, mode)) != NULL)
    {
        printf("error 1\n");
    }
    else
    {
        while (fread((buf + (*count)), sizeof(char), 1, in))
            (*count)++;
    }

    fclose(in);
    printf("successful\n");
}

void writing(char* buf, char *path = PATH3, char *mode = "a")
{
    FILE *out;
    int count = 0;

    if ((fopen_s(&out, path, mode)) != NULL)
    {
        printf("error 3\n");
    }
    else
    {
        if (buf != NULL)
        {
            while (buf[count])
            {
                fwrite(&buf[count], sizeof(char), 1, out);
                count++;
            }
        }
        else
        {
            printf(" the file %s become NULL ", path);
        }
    }

    fclose(out);
    printf("successful.", path);
}

int _tmain(int argc, _TCHAR* argv[])
{
    printf("program started\n");

    struct stat st;
    stat(PATH2, &st);
    int sizeArr = (int)st.st_size;

    char *chArr = (char *)calloc(sizeArr + 1, sizeof(char));
    chArr[sizeArr] = 0;

    unsigned long long int count = 0;
    reading(chArr, &count, PATH2, "rb");

    stat(PATHKEY, &st);
    int sizeKey = (int)st.st_size;
    char *chKey = (char *)calloc(sizeKey + 1, sizeof(char));
    unsigned long long int countKey = 0;
    reading(chKey, &countKey, PATHKEY);
    
    for (int n = 0; n < countKey; n++)
    {
        if (chKey[n] == '\r')
            delChar(&chKey[n]);
    }

    char *chBuf = (char *)calloc(countKey + 1, sizeof(char));
    unsigned long long int nBuf = 0;

    int countText = 0;
    char *chText = (char *)calloc(8 * sizeArr, sizeof(char));
    char *pText = chArr;
    unsigned long long int n = 0;
    int nTemp = 0;

    for (unsigned long long x = 0; x < count * 8 + 1; x++)
    {
        nTemp = x % 8;
        if ((chArr[x / 8] | byte_mask[nTemp]) != chArr[x / 8])
        {
            chText[countText] = chKey[nBuf - 1];
            countText++;
            nBuf = 0;
        }
        else
        {
            nBuf++;
        }
    }

    printf("\nwriting 1/2...");
    writing(NULL, PATH3, "w");

    printf("\nwriting 2/2...");
    writing(chText);

    printf("\n%s", chText);

    printf("\nthe program has worked.\n");

    return 0;
}
//СДЕЛАЛ!!!
