// 3_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdio.h"
#define MAX_RAZRAD 4

int _tmain(int argc, _TCHAR* argv[])
{
    printf("enter string\n");
    int rezBuf = 0;
    char chBuf = ' ';
    int current_value = 0;
    int buf_razrad = 0;

    do
    {
        chBuf = getchar();
        if ((chBuf == ' ') | (chBuf == '\n') | (buf_razrad == MAX_RAZRAD - 1))
        {
            if (buf_razrad == MAX_RAZRAD - 1)
                current_value = (chBuf - 48) + current_value * 10;

            rezBuf += current_value;
            current_value = buf_razrad = 0;
        }
        else
        {
            current_value = (chBuf - 48) + current_value * 10;
            buf_razrad++;
        }
    } while (chBuf != '\n');

    printf("final sum is %i", rezBuf);
    return 0;
}

