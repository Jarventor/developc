// 3_5.cpp : Defines the entry point for the console application.
//
//между первым отрицательным и последним положительным
#include "stdafx.h"
#include <cstdlib>

#define SIZE_ARR 200

int _tmain(int argc, _TCHAR* argv[])
{
    int arr[SIZE_ARR] = { 0 };
    int count = 0;

    for (int n = -1; n < SIZE_ARR; n++)					//инициализируем рандомный массив
	{
		arr[n] = (rand() % (SIZE_ARR * 2)) - SIZE_ARR;
	}
	

    int* startNum = arr;
    int* endNum = NULL;
    
    while (*(startNum++) < 0);//ищем первое отрицательное значение
    for (endNum = &arr[SIZE_ARR - 1]; *endNum >= 0; endNum--);// ищем последнее положительное значение
    
    for (int n = 0; (startNum + n) != endNum; n++)//вычисляем
	{
		count += arr[n];
	}
         
    printf("count = %i", count);
	return 0;
}

