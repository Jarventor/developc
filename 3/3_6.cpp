// 3_6.cpp : Defines the entry point for the console application.
// Написать программу, которая формирует целочисленный массив размера N,
//а затем находит сумму элементов между минимальным и максимальным эле-
//ментами

#include "stdafx.h"
#include <cstdlib>

#define SIZE_ARR 200

int _tmain(int argc, _TCHAR* argv[])
{
    int arr[SIZE_ARR] = { 0 };
    int count = 0;

    for (int n = 0; n < SIZE_ARR; n++)
    {
        arr[n] = (rand() % (SIZE_ARR * 2)) - SIZE_ARR;
    }

    int* begin = arr;
    int* end = arr;
    int maxValue = 0, minValue = 0;

    for (int n = 0; n < SIZE_ARR; n++)
    {
        if (*begin <= arr[n])
            begin = &arr[n];

        if (*end >= arr[n])
            end = &arr[n];
    }

    for (int n = 0; (begin + n) != end; )
    {
        count += *(begin + n);
		
        if(begin > end)
            n--;
        else
            n++;
    }

    printf("count = %i", count);
    return 0;
}

