// 3_9.cpp : Defines the entry point for the console application.
//Написать программу, определяющую в строке символьную последовательность
//максимальной длины


#include "stdafx.h"
#define SIZE_LEN 256

int _tmain(int argc, _TCHAR* argv[])
{
    char newSymbol, oldSymbol;
    char userString[SIZE_LEN] = "";
    char stringOut[SIZE_LEN] = "";
    unsigned short int nBuf = 0, count = 0;

    printf("enter string\n");

    if ((newSymbol = getchar()) != '\n')
        stringOut[nBuf] = newSymbol;

    do
    {
        oldSymbol = newSymbol;
        newSymbol = getchar();
        
        if (newSymbol == oldSymbol)
        {
            userString[nBuf] = oldSymbol;
            userString[++nBuf] = newSymbol;
        }
        else
        {
            if (nBuf > count)
            {
                count = nBuf;

                for (int n = 0; n < SIZE_LEN; n++)
				{
					stringOut[n] = userString[n];
					userString[n] = '\0';
				}
            }

            nBuf = 0;
        }
        
    } while (newSymbol != '\n');

    printf("longest word is '%s';\nIts length is %i sym\n", stringOut, count + 1);

    return 0;
}