// 3_10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define ARR_SIZE 256
#define NUM_CONST 48

void delSym(char* arr)
{
    while (*(arr++))
        *(arr - 1) = *(arr);
}

int _tmain(int argc, _TCHAR* argv[])
{
    char arr[ARR_SIZE] = "";
    char *arrPointers[ARR_SIZE] = { arr, 0 };
    char sym;
    int nBuf = -1;
    int nBufPointer = 0;
    printf("enter string\n");
    
    while ((arr[++nBuf] = getchar()) != '\n')
        if (arr[nBuf] == ' ')
            arrPointers[nBufPointer++] = &arr[nBuf + 1];
    
    printf("enter num\n");
    
    nBuf = getchar() - NUM_CONST;

    if ((nBuf < 1) | (nBuf > nBufPointer))
        printf("error num\n");
    else
        for (int n = 1; *(arrPointers[nBuf - 2]) != ' '; delSym(arrPointers[nBuf - 2]));

    printf("string is %s", arr);

	return 0;
}

