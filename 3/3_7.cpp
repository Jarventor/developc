// 3_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define MAX 256
short int maxValues = 0;

int _tmain(int argc, _TCHAR* argv[])
{
    char buf[MAX];
    short int counts[MAX] = { 0 };

    printf("Insert the line\n");
    scanf_s("%s", buf, MAX);

    for (int i = 0; buf[i]; i++)
        counts[buf[i]]++;

    do
    {
        maxValues = 0;
        for (int i = 0; i < MAX; i++)
            if (counts[i] > maxValues)
                maxValues = counts[i];

        for (int i = 0; i < MAX; i++)
            if ((counts[i] == maxValues) && (counts[i] != 0))
            {
                printf("%c - %d\n", i, counts[i]);
                counts[i] = 0;
            }
    } while (maxValues > 0);

	return 0;
}

