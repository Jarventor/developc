// 3_2.cpp : Defines the entry point for the console application.
//
//#include <Windows.h>
#include "stdafx.h"
#include <cstdlib>
#include "Windows.h"

#define SUM_OF_NUMBERS 10
#define LOWER_BOUNDS_OF_NUMBERS 48
#define SUM_OF_LETTER_GROW 26
#define LOWER_BOUND_OF_LETTER_GROW 65
#define SUM_OF_LETTER_LITTLE 26
#define LOWER_BOUND_OF_LETTER_LITTLE 97
#define SPACER 32
#define ARR_SIZE 45

char* findPosition(char* ch, bool startOrEnd = false)
{

    while (*(ch + 1))
    {
        if ((startOrEnd) ? ((*ch == ' ') & (*(ch + 1) != ' ')) : (*ch != ' ') & (*(ch + 1) == ' '))
            return (ch + 1);
        ch++;
    }
    return NULL;
}

char charRandom(int index = 0)
{
    int size = 0;
    int x, y;

    switch (index)
    {
    case 0:
        x = LOWER_BOUND_OF_LETTER_LITTLE;
        y = SUM_OF_LETTER_LITTLE;
        break;
    case 1:
        x = LOWER_BOUND_OF_LETTER_GROW;
        y = SUM_OF_LETTER_GROW;
        break;
    case 2:
        x = LOWER_BOUNDS_OF_NUMBERS;
        y = SUM_OF_NUMBERS;
    case 3:
        return SPACER;
    }
    return char((rand() % (y)) + x);
}

int _tmain(int argc, _TCHAR* argv[])
{
    char stringUser[ARR_SIZE] = "";

    for (int n = 0; n + 1 < ARR_SIZE; n++)
        stringUser[n] = charRandom((rand()) % (4));

    printf("stroka = '%s'\n", &stringUser);
    char* startWord = stringUser;
    int sumWords = 1;
    char* endWord = findPosition(stringUser);
    int lenWord = 0;

    while ((endWord = findPosition(startWord)) != NULL)
    {
        sumWords++;
        lenWord = 1;
        printf("in word '");
        
        while (startWord != endWord)
        {
            lenWord++;
            printf("%c", *(startWord++));
        }
        printf("' length word is %i sym\n", lenWord - 1);
        startWord = findPosition(startWord, true);
    }
 
    printf("final sum words = %i\n", sumWords);
    return 0;
}

