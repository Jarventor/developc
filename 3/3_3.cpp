#include "stdafx.h"
#define SIZE_LEN 256

int _tmain(int argc, _TCHAR* argv[])
{
    printf("enter string\n");
    int count = 0;
    char userString[SIZE_LEN] = "";
    int nBuf = 0;
    int currentPosition = 0;
    char *ptr = userString;

    do
    {
        userString[currentPosition] = getchar();
        nBuf++;

        if ((userString[currentPosition] == ' ') || (userString[currentPosition] == '\n'))
        {
            if (nBuf >= count)
            {
                ptr = &userString[currentPosition - nBuf];
                count = nBuf;
                nBuf = 0;
            }
        }

    } while (userString[currentPosition++] != '\n');

    for (int n = 0; n < count; n++)
    {
        printf("%c", *(ptr + n));
    }

    printf("   is %i symbols", count);
    return 0;
}