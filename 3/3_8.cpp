// 3_8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define SIZE 256

int _tmain(int argc, _TCHAR* argv[])
{
    printf("enter string\n");
    char sym;
    char arr[SIZE][SIZE] = { '\0' };

    int sumWord = 0, sumBit = 0;
    do
    {
        sym = getchar();
        if ((sym == ' ') | (sym == '\n'))
        {
            sumWord++;
            sumBit = 0;
        }
        else
        {
            arr[sumWord][sumBit++] = sym;
        }
    } while (sym != '\n');
    
    printf("enter num word\n");
    scanf_s("%i", &sumBit);

    if ((sumBit < 1) | (sumBit > sumWord))
    {
        printf("error num\n");
    }
    else
    {
        printf("this word is '%s'", arr[sumBit - 1]);
    }

    return 0;
}
