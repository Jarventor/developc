#include "stdafx.h"

#define BAD_NUM -1

int main_1_2()
{
    int ch = BAD_NUM;
    int m = BAD_NUM;
    int s = BAD_NUM;
    printf("\nis your time?(chch:mm:ss)\n");
    scanf("%i:%i:%i", &ch, &m, &s);
    
    if (ch < 0 || ch > 24 || m < 0 || m > 59 || s < 0 || s > 59)
    {
        printf("bad time");
        return 0;
    }

    if (ch < 6)
        printf("good nigth!\n");
    else if (ch < 10)
        printf("good morning!\n");
    else if (ch < 13)
        printf("good day!\n");
    else if (ch < 22)
        printf("good evening!\n");
    else if (ch < 24)
        printf("good nigth!\n");

    return 0;
}

