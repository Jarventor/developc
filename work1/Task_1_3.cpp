#include "stdafx.h"

#define DEGREE 'D'
#define RAD 'R'
#define INT_CONVERT_D 0.0175
#define INT_CONVERT_R 57,2958

int main_1_3()
{
    float ugol = 0.0;
    char anChar = ' ';
    printf("\npress number in format '5.0D;' or '5.0R'\n");
    scanf("%f%c", &ugol, &anChar);
    
    if (anChar == DEGREE)
    {
        ugol = INT_CONVERT_D * ugol;
        printf("it is %f radian", ugol);
    }
    else if(anChar == RAD)
    {
        ugol = INT_CONVERT_R * ugol;
        printf("it is %f degree", ugol);
    }
    return 0;
}

