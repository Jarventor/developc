#include "stdafx.h"
#define oneFeetInInches 12
#define oneFeetInSentimeter 2.54
int main_1_4()
{
    int feet = -1;
    int inches = -1;
    printf("press feet and inches in formate 'ff.dd'\n");
    scanf("%d.%d", &feet, &inches);
    
    if (feet <= 0 || inches <= 0)
    {
        printf("bad data input\n");
        return 0;
    }
    float res = oneFeetInSentimeter * (oneFeetInInches * feet + inches);
    printf("it is %.1f centimeter", res);
    return 0;
}

