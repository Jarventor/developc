#include "stdafx.h"
#include <string>
#include <fstream>
#include <iostream>
#define DEFAULT_CONSOLS_WIDTH 80

int main_1_5()
{
    char strForOut [DEFAULT_CONSOLS_WIDTH]= "";
    printf("press string\n");
    scanf("%s", &strForOut);
    int nStrLen = strlen(strForOut);

    if(nStrLen > DEFAULT_CONSOLS_WIDTH)
    {   
        printf("bad data input. long string\n");
    }
    else if (nStrLen <= 0)
    {
        printf("bad data input. empty string\n");
    }
    else
    {
        int n = DEFAULT_CONSOLS_WIDTH /2 + nStrLen / 2;
        printf("%*s", n, strForOut);
    }
    return 0;
}

