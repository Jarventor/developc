// 4_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>

#define SIZE 256

void findNext(char*ch, bool trend = true)
{
    while (*((trend)?(ch++) : (ch--)) == ' ');
}

int _tmain(int argc, _TCHAR* argv[])
{
    printf("enter string\n");
    unsigned short int sumWord = 0, sumBit = 0;
    char arr[SIZE] = "";
    char *pBegin = arr;

    while ((arr[sumBit++] = getchar()) != '\n')
        if (arr[sumBit - 1] == ' ')
            sumBit--;

    char *pEnd = &(arr[sumBit - 2]);

    do
    {
        if (*pBegin != *pEnd)
        {
            printf("palindrom -\n");
            return 0;
        }
    } while (pBegin++ < pEnd--);

    printf("palindrom +\n");
    return 0;
}
