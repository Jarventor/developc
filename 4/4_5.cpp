#include "stdafx.h"
#include <string.h>
#define SIZE_LEN 256

int _tmain(int argc, _TCHAR* argv[])
{
    char charArr[SIZE_LEN][SIZE_LEN] = { 0 };
    char *pArr[SIZE_LEN] = { charArr[0], 0 };
    unsigned short int nString = 0, nBitMax = 0;

    char* read = NULL;
    FILE* in;
    fopen_s(&in, "G:/Labs/all_solutions/Debug/input.txt", "r+");

    if (in)
    {
        do
        {
            read = fgets(charArr[nString], sizeof(charArr[nString]), in);

            pArr[nString] = charArr[nString];

            if (strlen(pArr[nString]) > nBitMax)
                nBitMax = strlen(pArr[nString]);

            nString++;

        } while (read != NULL);
    }

    fclose(in);
    FILE* out;
    fopen_s(&out, "G:/Labs/all_solutions/Debug/output.txt", "wb");

    for (int m = 0; m <= nBitMax; m++)
        for (int n = 0; n < nString; n++)
            if (strlen(pArr[n]) == m)
                fprintf(out, "%s\n", pArr[n]);

    fclose(out);
    return 0;