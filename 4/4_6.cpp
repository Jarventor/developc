// 4_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define MAX 256

int _tmain(int argc, _TCHAR* argv[])
{
    char* old;
    char* young;
    char name[MAX][MAX] = { 0 };
    int age[MAX] = { 0 };
    int members, oldest = 0, youngest = 100;
    printf("Please enter count of family members\n");
    scanf_s("%d", &members);

    for (int i = 0; i < members; i++)
    {
        printf("Please enter the name of %d family member\n", i + 1);
        scanf_s("%s", name[i], MAX);
        printf("Please enter the age\n");
        scanf_s("%d", &age[i]);
        if (age[i] > oldest)
        {
            oldest = age[i];
            old = name[i];
        }
        if (age[i] < youngest)
        {
            youngest = age[i];
            young = name[i];
        }
    }
    printf("The oldest family member is %s %d years old\nThe youngest family member is %s %d years old\n", old, oldest, young, youngest);
    return 0;
}