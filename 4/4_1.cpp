// 4_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#define SIZE_LEN 256

int _tmain(int argc, _TCHAR* argv[])
{
    char charArr[SIZE_LEN][SIZE_LEN] = { '\n' };
    char *pArr[SIZE_LEN] = { charArr[0], 0 };
    unsigned short int nString = 0, nBit = 0, nBitMax = 0;

    printf("enter string\n");

    do
    {
        charArr[nString][nBit++] = getchar();

        if (charArr[nString][nBit - 1] == '\n')
        {
            pArr[nString++] = charArr[nString];
 
            if (nBitMax < nBit)
                nBitMax = nBit;

            nBit = 0;

            if (*pArr[nString - 1] == '\n')
                break;
        }

    } while (true);

        for (int m = 0; m <= nBitMax; m++)
            for (int n = 0; n < nString; n++)
                if (strlen(pArr[n]) == m)
                    printf("%s", pArr[n]);

    return 0;
}